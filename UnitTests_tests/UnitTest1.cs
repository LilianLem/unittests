using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTests;

namespace UnitTests_tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            FirstClass.Multiplier(42);
            
            Assert.AreEqual(FirstClass.Multiplier(42), 84);
            Assert.AreNotEqual(FirstClass.Multiplier(42), 42);
        }
    }
}